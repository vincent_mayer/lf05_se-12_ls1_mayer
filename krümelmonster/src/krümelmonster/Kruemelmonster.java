import java.util.Scanner;

public class Kruemelmonster {

	public static void main(String[] args) {
		
		int keksenachfressen;
		int kekse = 100;
		int anzahl;
		Scanner tastatur = new Scanner(System.in);
		
		Kruemelmonster.sprich();
			
		kekse = Kruemelmonster.friss5(kekse);
		System.out.println("Anzahl Kekse nach dem Fressen von 5 Keksen: " + kekse);
		
		kekse = Kruemelmonster.bring10(kekse);
		System.out.println("Anzahl Kekse nach dem Bringen von 10 Keksen: " + kekse);

		System.out.println("Wie viele Kekse bringt der Nikolaus? ");
		anzahl = tastatur.nextInt();	
		
		kekse = Kruemelmonster.nikolaus(kekse, anzahl);
		System.out.println("Anzahl Kekse nach dem Nikolaus: " + kekse);
		
		keksenachfressen = Kruemelmonster.frissAlleKekse (kekse);
		System.out.println("Anzahl Kekse nach dem fressen: " + keksenachfressen);
		
	}
	
	
	public static void sprich() {
		System.out.println("Ich will Kekseeeee!!!");
	}
	
	public static int friss5(int a) {
		a = a-5;
		return a;
	}
	
	public static int bring10(int b) {
		b = b+10;
		return b;
	}
	
	public static int nikolaus(int kekse, int anzahl) {
		kekse = kekse + anzahl;
		return kekse;
	}
	
	//Das Krümelmonster hat Hunger und frisst alle Kekse
	//Implementieren Sie die Methode  frissAlleKekse()
	
	public static int frissAlleKekse(int kekse) {
		kekse = 0;
		return kekse ;
	}
	
}
	
